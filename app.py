from functools import wraps
import werkzeug.exceptions
from flask import Flask, render_template, request, redirect, url_for, session, flash, jsonify
import sqlalchemy
from sqlalchemy.sql import func
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from werkzeug.security import generate_password_hash, check_password_hash
import pymysql

app = Flask(__name__, template_folder='templates')
app.secret_key = 'igor'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:igor1234@localhost/company'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

migrate = Migrate(app, db)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(80), unique=True)
    email = db.Column(db.String(120), unique=True)
    password = db.Column(db.String(200))

    def __init__(self, username, email, password):
        self.username = username
        self.email = email
        self.password = password

    def check_password(self, password):
        return check_password_hash(self.password, password)

    @staticmethod
    def hash_password(password):
        return generate_password_hash(password)


class Department(db.Model):
    __tablename__ = 'department'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    address = db.Column(db.String(50))
    db.relationship('Employee', backref='employee', lazy=True)

    def __init__(self, name, address):
        self.name = name
        self.address = address


class Employee(db.Model):
    __tablename__ = 'employee'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    date_of_birth = db.Column(db.Date, nullable=False)
    salary = db.Column(db.Float, nullable=False)
    department_id = db.Column(db.Integer, db.ForeignKey('department.id'),
                              nullable=False)

    def __init__(self, name, date_of_birth, salary, department_id):
        self.name = name
        self.date_of_birth = date_of_birth
        self.salary = salary
        self.department_id = department_id


# Check if user logged in
def is_admin(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'user' in session and session['user'] == 'admin':
            return f(*args, **kwargs)
        else:
            flash('You Have No Rights To Edit DB', 'danger')
            return redirect(url_for('view'))
    return wrap


@app.route('/')
def home():
    return render_template('index.html')


@app.route('/create', methods=['POST', 'GET'])
def create_user():
    if request.method == 'POST':
        usr = request.form['name']
        if User.query.filter_by(username=usr).first():
            flash('User already exists', 'danger')
            return redirect(url_for('create_user')), 404
        else:
            email = request.form['email']
            pwd = request.form['password']
            urs = User(usr, email, User.hash_password(pwd))
            db.session.add(urs)
            db.session.commit()
            flash('User Was Created Successfully', 'success')
        return redirect(url_for('login'))
    return render_template('create.html')


@app.route('/login', methods=['POST', 'GET'])
def login():
    if request.method == 'POST':
        usr = request.form['name']
        found_user = User.query.filter_by(username=usr).first()
        if found_user:
            if User.check_password(found_user, request.form['password']):
                session['user'] = usr
                flash('Login Successful', 'success')
                return redirect(url_for('admin')) if session['user'] == 'admin' else redirect(url_for('view'))
            else:
                flash('Password incorrect', 'danger')
        else:
            flash('User Not Found', 'danger')
            return render_template('login.html')
    else:
        if 'user' in session:
            flash('Already Logged In!', 'danger')
            return redirect(url_for('view'))
    return render_template('login.html')


@app.route('/view', methods=['GET', 'POST'])
def view():
    if 'user' in session:
        deps = Department.query.all()
        avg_salary = []
        for dep in deps:
            avg_salary.append(Employee.query.with_entities(func.avg(Employee.salary).label('average')).filter_by(department_id=dep.id).first()[0])
        employees = Employee.query.all()
        if deps:
            return render_template('view.html', Department=Department, depatrments=deps, Employee=Employee, avg_salary=avg_salary, employees=employees)
        elif deps:
            flash('No Employees Found', 'danger')
            return render_template('view.html', Department=Department, depatrments=deps, Employee=Employee, avg_salary=avg_salary)
        elif employees:
            flash('No Departments Found', 'danger')
            return render_template('view.html', Department=Department, employees=employees, Employee=Employee)
        else:
            flash('No Departments Or Employees Were Found')
            return render_template('view.html')
    else:
        flash('You Are Not Logged In!', 'danger')
        return redirect(url_for('login'))


@app.route('/admin', methods=['GET', 'POST'])
@is_admin
def admin():
    return render_template('admin.html')


@app.route('/admin/departments/', methods=['GET', 'POST'])
@is_admin
def admin_deps():
    deps = Department.query.all()
    avg_salary = []
    for dep in deps:
        avg_salary.append(Employee.query.with_entities(func.avg(Employee.salary).label('average')).filter_by(
            department_id=dep.id).first()[0])
    if deps:
        return render_template('admin_departments.html', depatrments=deps, Employee=Employee, avg_salary=avg_salary)
    else:
        flash('No messages found', 'danger')
    return render_template('admin_departments.html')


@app.route('/admin/add_department', methods=['GET', 'POST'])
@is_admin
def add_department():
    if request.method == 'POST':
        name = request.form['name']
        found_dep = Department.query.filter_by(name=name).first()
        if found_dep:
            flash('Department already exists', 'danger')
        else:
            address = request.form['address']
            d = Department(name, address)
            db.session.add(d)
            db.session.commit()
            flash('Department Added Successfully', 'success')
        return redirect(url_for('admin_deps'))
    return render_template('add_department.html')


@app.route('/admin/edit_department/<int:id>', methods=['GET', 'POST'])
@is_admin
def edit_department(id):
    dep = Department.query.filter_by(id=id).first()
    if request.method == 'POST':
        dep.name = request.form['name']
        dep.address = request.form['address']
        db.session.commit()
        flash('Department edited Successfully', 'success')
        return redirect(url_for('admin_deps'))

    return render_template('edit_department.html', department=dep)


# Delete Article
@app.route('/admin/departments/delete_department/<int:id>', methods=['GET', 'POST'])
@is_admin
def delete_department(id):
    Department.query.filter_by(id=id).delete()
    db.session.commit()
    return redirect(url_for('admin_deps'))


@app.route('/admin/employees/', methods=['GET', 'POST'])
@is_admin
def admin_emps():
    employees = Employee.query.all()
    return render_template('admin_employees.html', Department=Department, employees=employees, Employee=Employee)


@app.route('/admin/add_employee', methods=['GET', 'POST'])
@is_admin
def add_employee():
    if request.method == 'POST':
        name = request.form['name']
        date_of_birth = request.form['date_of_birth']
        found_emp = Employee.query.filter_by(name=name, date_of_birth=date_of_birth).first()
        if found_emp:
            flash('Employee Already In base', 'danger')
        else:
            salary = request.form['salary']
            department_id = request.form['department_id']
            emp = Employee(name, date_of_birth, salary, department_id)
            db.session.add(emp)
            db.session.commit()
            flash('Employee Added Successfully', 'success')
        return redirect(url_for('admin_emps'))
    return render_template('add_employee.html')


@app.route('/admin/edit_employee/<int:id>', methods=['GET', 'POST'])
@is_admin
def edit_employee(id):
    emp = Employee.query.filter_by(id=id).first()
    if request.method == 'POST':
        emp.name = request.form['name']
        emp.date_of_birth = request.form['date_of_birth']
        emp.salary = request.form['salary']
        emp.department_id = request.form['department_id']
        db.session.commit()
        flash('Employee edited successfully', 'success')
        return redirect(url_for('admin_emps'))
    return render_template('edit_employee.html', employee=emp)


@app.route('/admin/employees/delete_employee/<int:id>', methods=['GET', 'POST'])
@is_admin
def delete_employee(id):
    # Create cursor
    Employee.query.filter_by(id=id).delete()
    db.session.commit()
    return redirect(url_for('admin_emps'))


@app.route('/logout', methods=['POST', 'GET'])
def logout():
    flash('You Logged Out', 'success')
    session.pop('user', None)
    return redirect(url_for('login'))


if __name__ == '__main__':
    app.run(debug=True)
