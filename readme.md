# Final project for Epam Python course.

### I created two different projects:
1. Flask app, that can be used to manage departments and employees(Documentation is below in this README file).
2. [Rest API](https://gitlab.com/igorshevchuk540/flask_restful), using Flask-RESTful. This project is in another repository. You can access it by tapping Hyperlink<br>
<br><br>
### Now lets begin with the web app documentation.
To use my app you can download it from my gitlab repository, then run the following command: ```pip install -r requirements.txt```
to install dependencies, needed to start the app(File is also in the working directory). After that simply run fule ```app.py```.
##### Below is given structure of my application.
1. In folder ```/migrations``` lie migration files for *DB*, used in this project.
2. ```/templates``` folder consists of different *.html* files, that are used for routing through my app.
3. ```App.py``` is main file of my app.
4. ```dependencies.txt``` contains all packages, needed to run the app.
5. ```test.py``` has Unit tests to test my app.

In this app you can create different users, login, logout. While being logged, you can view all departments and employees, that work there.<br>
While being logged in as *admin* user, you have access to edit *DB* sequences.