from app import app
import unittest


class FlaskTest(unittest.TestCase):
    def test_index(self):
        tester = app.test_client(self)
        response = tester.get('/')
        self.assertEqual(response.status_code, 200)

    def test_create(self):
        tester = app.test_client(self)
        response = tester.get('/create')
        self.assertEqual(response.status_code, 200)

    def test_login(self):
        tester = app.test_client(self)
        response = tester.post(
            '/login',
            data={'name': 'Igor', 'password': 1234},
        )
        assert response.status_code == 200


if __name__ == '__main__':
    unittest.main()
